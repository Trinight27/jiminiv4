
class Assaisonnement :


    def __init__(self,unNom : str ,unPrix : float):
        self.setNom(unNom)
        self.setPrix(unPrix)

    def setNom(self,valeur) -> None:
        self.__nom=valeur

    def setPrix(self,valeur) -> None:
        self.__prix=valeur

    def getNom(self) -> str:
        return self.__nom

    def getPrix(self) -> float:
        return self.__prix

    def __str__(self):
        return str(self.__nom)