from __future__ import annotations
from insecte import Insecte
from  assaisonnement import  Assaisonnement

class Boite:
    """Classe Boite
    Permet de créer une instance de Boite
    Attributs d'instance
    --------------------
    assaisonnement : str
        nom de l'assaissonnement
    poids : int
        poids de la boite en g
    prix : float
        prix de la boite
    linsecte : Insecte
        insecte contenu dans la boite
    """

    def __init__(self,unPoids: int, unInsecte: Insecte, unAssaisonnement : Assaisonnement):
        # TODO 3 : implémenter le code du constructeur permettant la valorisation des attributs.
        self.setPoids(unPoids)
        self.setLInsecte(unInsecte)
        self.setAssaisonnement(unAssaisonnement)

    def setPoids(self, valeur: int) -> None:
        """Setter sur l'attribut poids
        :param valeur : valeur pour le poids
        """
        self.__poids = valeur

    def setLInsecte(self, valeur: Insecte) -> None:
        """Setter sur l'attribut linsecte
        :param valeur : valeur pour le linsecte
        """
        self.__linsecte = valeur

    def setAssaisonnement(self,valeur : Assaisonnement) -> None:
        self.__lass = valeur


    def getLInsecte(self) -> Insecte:
        '''
        Getter sur l'attribut linsecte
        :return: l'insecte
        '''
        return self.__linsecte

    def getAssaisonnement(self) -> Assaisonnement:
        return self.__lass

    def prix(self):
        return (self.__linsecte.getPrixAuKilo()*self.__poids)/1000+self.__lass.getPrix()


    # TODO 4: définir la méthode permettant de générer l'affichage d'une instance de Boite au format str
    def __str__(self) -> str:
        return "Boîte de " + str(self.__linsecte) + " (" + str(self.__poids) + "g) - assaisonnées " + str(
            self.__lass.getNom())+" - Prix de la boite : "+str(self.prix())+" €"
