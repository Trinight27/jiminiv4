"""
Programme Jimini's : manger des insectes comestibles
Salés, sucrés ou naturés, les insectes de chez Jimini's suraont vous régaler tout au long de la journée
"""
from insecte import Insecte
from boite import Boite

# Programme principal

# création des insectes
# TODO 1 : instancier 3 insectes : le criquet (rnj : 28), le grillon (rnj : 25), le molitor (rnj : 28)
# TODO 2 : afficher les insectes (nom)
lesAssaisonnement =["mangue douce","oignon fumé","paprika","poivre & tomates séchées","curry fruité","à la grecque","sésame & cumin","ail & fines herbes","soja impérial"]

insectes = [Insecte("criquet", 28), Insecte("grillon", 25), Insecte("molitor", 28)]
for i in range(len(insectes)):
    print(str(insectes[i]))

# collection de Boite
lesBoites = [Boite("mangue douce", 6, 14, insectes[1]), Boite("oignon fumé", 5, 14, insectes[1]),
             Boite("paprika", 3, 10, insectes[0]),
             Boite("poivre & tomates séchées", 4, 10, insectes[0]), Boite("curry fruité", 5, 10, insectes[0]),
             Boite("à la grecque", 4, 10, insectes[0]),
             Boite("sésame & cumin", 9, 18, insectes[2]), Boite("ail & fines herbes", 3, 18, insectes[2]),
             Boite("soja impérial", 7, 18, insectes[2])]
print("Contenu du catalogue")
for index in range(len(lesBoites)):
    print(str(lesBoites[index]))



# TODO 3 : instancier les boites en les ajoutant à la collection de Boites.
# TODO 4 : Produire l'affichage attendu.
