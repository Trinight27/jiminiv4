from typing import List
from boite import Boite

class Catalogue:

    def __init__(self,uneAnnee : int):
        self.setAnnee(uneAnnee)
        self.__lesBoites = []

    def setAnnee(self,valeur) -> None:
        self.__annee = valeur

    def getAnnee(self) -> int:
        return self.__annee

    def ajouterBoite(self, uneBoite : Boite) -> None:
        self.__lesBoites.append(uneBoite)

    def getLesBoites(self) -> List:
        return self.__lesBoites

    def laPlusCherAuKilo(self) -> Boite:
        if len(self.__lesBoites) == 0:
            boiteMax = None
        else:
            boiteMax = self.__lesBoites[0]
            maxiPrix = self.__lesBoites[0].prixAuKilo()
            for index in range(len(self.__lesBoites)):
                if self.__lesBoites[index].prixAuKilo() > maxiPrix:
                    maxiPrix = self.__lesBoites[index].prixAuKilo()
                    boiteMax = self.__lesBoites[index]
        return boiteMax

    def laMoinCherAuKilo(self) -> Boite:
        if len(self.__lesBoites) == 0:
            boiteMin=None
        else:
            boiteMin = self.__lesBoites[0]
            minPrix = self.__lesBoites[0].prixAuKilo()
            for index in range(len(self.__lesBoites)):
                if self.__lesBoites[index].prixAuKilo() < minPrix:
                    minPrix = self.__lesBoites[index].prixAuKilo()
                    boiteMin = self.__lesBoites[index]
        return boiteMin

    def lesBoitesDepuisNomInsecte(self, nomInsecte: str) -> List[Boite]:
        """ Retourne la liste des boites contenant l'insecte dont le nom est passé en paramètre
        :param nomInsecte: nom de l'insecte
        :return: liste de Boite
        """
        if len(self.__lesBoites) == 0:
            contientInsecte=None
        else:
            contientInsecte = []
            for index in range(len(self.__lesBoites)):
                if self.__lesBoites[index].getLInsecte().getNom() == nomInsecte:
                    contientInsecte.append(self.__lesBoites[index])
        return contientInsecte