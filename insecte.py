class Insecte:
    """Classe Insecte
    Permet de créer une instance d'Insecte
    Attributs d'instance
    --------------------
    nom : str
        nom de l'animal
    rnj : int
        repère nutritionnel journalier (rnj) correspondant à la teneur en énergie et en macro-nutriments.
    """

    def __init__(self, unNom: str, valRnj : int, unPrixAuKilo : float):
        """
        Constructeur de la classe Insecte
        """
        self.setNom(unNom)
        self.setRnj(valRnj)
        self.setPrixAuKilo(unPrixAuKilo)

    def setNom(self, valeur : str) -> None :
        """Setter sur l'attribut nom
        :param valeur : valeur pour le nom
        """
        self.__nom = valeur

    def setRnj(self, valeur : int) -> None:
        """Setter sur l'attribut rnj
        :param valeur : valeur pour le rnj
        """
        self.__rnj = valeur

    def setPrixAuKilo(self,valeur) -> None:
        self.__prixAuKilo=valeur


    def getNom(self) -> str:
        return self.__nom

    def getPrixAuKilo(self) -> float:
        return self.__prixAuKilo

    # TODO 2: définir la méthode permettant de générer l'affichage d'une instance d'Insecte au format str
    def __str__(self) -> str:
        return self.__nom
