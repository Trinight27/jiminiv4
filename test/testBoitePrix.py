import unittest
from boite import Boite
from insecte import Insecte
from assaisonnement import Assaisonnement

class testBoitePrixAuKilo(unittest.TestCase):
    """Classe de test de la classe Boite
    """

    def setUp(self):
        """Initialisation du contexte de test
        """
        self.listeInsect = {}
        self.listeInsect["criquet"] = Insecte("criquet", 28, 10)
        self.listeInsect["grillon"] = Insecte("grillon", 50 , 0)


    def testPrix(self):
        """Teste de la methode de prix
        """
        self.uneBoite = Boite(1, self.listeInsect["criquet"],Assaisonnement("à la grecque",5))
        self.assertEqual(self.uneBoite.prix(), 5.01, "erreur calcul prix au kilo")

    def testPrixNull(self):
        """Teste de la methode de prix si l' insect na pas de prix au kilo
        """
        self.uneBoite = Boite(1, self.listeInsect["grillon"],Assaisonnement("à la grecque",5))

        self.assertEqual(self.uneBoite.prix(), 5, "erreur calcul prix au kilo")
