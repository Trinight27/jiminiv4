import unittest
from boite import Boite
from catalogue import Catalogue
from insecte import Insecte

class testCatalogueAjouterBoite(unittest.TestCase):
    """Classe de test de la classe Catalogue
    """

    def setUp(self):
        """Initialisation du contexte de test
        """
        self.unCatalogue = Catalogue(2021)
        listeInsect = {}
        listeInsect["criquet"] = Insecte("criquet", 28)
        self.uneBoite = Boite("à la grecque", 0.15, 10, listeInsect["criquet"])


    def testAjouteBoiteLongueur(self):
        """Teste de la methode ajouterBoite
        """
        longueur=len(self.unCatalogue.getLesBoites())
        self.unCatalogue.ajouterBoite(self.uneBoite)
        self.assertEqual(len(self.unCatalogue.getLesBoites()),longueur+1 , "La liste n'est pas plus grande")

    def testAjouteBoiteLaBonneBoite(self):
        """Teste de la methode ajouterBoite
        """
        self.unCatalogue.ajouterBoite(self.uneBoite)
        self.assertTrue(self.uneBoite in self.unCatalogue.getLesBoites(),"il ajoute la mauvaise boite ")