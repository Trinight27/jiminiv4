import unittest
from boite import Boite
from insecte import Insecte
from catalogue import Catalogue


class testCatalogueLaPlusCherAuKilo(unittest.TestCase):
    """Classe de test de la fonction laMoinsChezAuKilo
    """

    def setUp(self):
        """Initialisation du contexte de test
        """
        self.unCatalogue = Catalogue(2021)
        self.listeInsect = {}
        self.listeInsect["criquet"] = Insecte("criquet", 28)
        self.listeInsect["grillon"] = Insecte("grillon", 25)
        self.listeInsect["molitor"] = Insecte("molitor", 28)
        self.uneBoite = Boite("à la grecque", 0.15, 10, self.listeInsect["criquet"])


    def testLaPlusCherAuKiloPlusieursBoites(self):
        """Teste de la methode de prix au kilo
        """
        self.unCatalogue.ajouterBoite(Boite("mangue douce", 0.10, 14, self.listeInsect["grillon"]))
        self.unCatalogue.ajouterBoite(Boite("oignon fumé", 0.10, 14, self.listeInsect["grillon"]))
        self.unCatalogue.ajouterBoite(Boite("paprika", 0.10, 10, self.listeInsect["criquet"]))
        self.unCatalogue.ajouterBoite(Boite("poivre & tomates séchées", 0.10, 10, self.listeInsect["criquet"]))
        self.unCatalogue.ajouterBoite(Boite("curry fruité", 0.01, 10, self.listeInsect["criquet"]))
        self.unCatalogue.ajouterBoite(Boite("à la grecque", 0.15, 10, self.listeInsect["criquet"]))
        self.unCatalogue.ajouterBoite(Boite("sésame & cumin", 0.10, 18, self.listeInsect["molitor"]))
        self.unCatalogue.ajouterBoite(Boite("ail & fines herbes", 0.10, 18, self.listeInsect["molitor"]))
        self.unCatalogue.ajouterBoite(Boite("soja impérial", 3, 10, self.listeInsect["molitor"]))

        self.assertEqual(round(self.unCatalogue.laPlusCherAuKilo().prixAuKilo(), 2), 300,
                         "la boite la Plus cher n'est pas celle souhaiter")

    def testLaPlusCherAuKiloAucuneBoite(self):
        """Teste de la methode de prix au kilo
        """
        self.assertEqual(self.unCatalogue.laPlusCherAuKilo(), None, "la boite la Plus cher n'est pas celle souhaiter")
