import unittest
from boite import Boite
from insecte import Insecte
from catalogue import Catalogue


class testCatalogueLesBoitesDepuisNomInsecte(unittest.TestCase):
    """Classe de test de la fonction LesBoitesDepuisNomInsecte
    """

    def setUp(self):
        """Initialisation du contexte de test
        """
        self.unCatalogue = Catalogue(2021)
        self.listeInsect = {}
        self.listeInsect["criquet"] = Insecte("criquet", 28)
        self.listeInsect["grillon"] = Insecte("grillon", 25)
        self.listeInsect["molitor"] = Insecte("molitor", 28)
        self.uneBoite = Boite("mangue douce", 0.10, 14, self.listeInsect["grillon"])


    def testLesBoitesDepuisNomInsectePlusieursBoites(self):
        """Teste de la methode de LesBoitesDepuisNomInsecte avec des boites
        """


        self.unCatalogue.ajouterBoite(Boite("mangue douce", 0.10, 14, self.listeInsect["grillon"]))
        self.unCatalogue.ajouterBoite(Boite("oignon fumé", 0.10, 14, self.listeInsect["grillon"]))
        self.unCatalogue.ajouterBoite(Boite("paprika", 0.10, 10, self.listeInsect["criquet"]))
        self.unCatalogue.ajouterBoite(Boite("poivre & tomates séchées", 0.10, 10, self.listeInsect["criquet"]))
        self.unCatalogue.ajouterBoite(Boite("curry fruité", 0.01, 10, self.listeInsect["criquet"]))
        self.unCatalogue.ajouterBoite(Boite("à la grecque", 0.15, 10, self.listeInsect["criquet"]))
        self.unCatalogue.ajouterBoite(Boite("sésame & cumin", 0.10, 18, self.listeInsect["molitor"]))
        self.unCatalogue.ajouterBoite(Boite("ail & fines herbes", 0.10, 18, self.listeInsect["molitor"]))
        self.unCatalogue.ajouterBoite(Boite("soja impérial", 3, 10, self.listeInsect["molitor"]))

        lesBonnesBoites=[self.unCatalogue.getLesBoites()[0],self.unCatalogue.getLesBoites()[1]]
        self.assertEqual(self.unCatalogue.lesBoitesDepuisNomInsecte("grillon"), lesBonnesBoites ,
                         "les boites ne sont pas celles souhaiter")

    def testLesBoitesDepuisNomInsecteAucuneBoite(self):
        """Teste de la methode LesBoitesDepuisNomInsecte sans boite
        """
        nomInsecte="grillon"
        self.assertEqual(self.unCatalogue.lesBoitesDepuisNomInsecte(nomInsecte), None, "les boites ne sont pas celles souhaiter")

    def testLesBoitesDepuisNomInsecteNexistepas(self):
        """Teste de la methode LesBoitesDepuisNomInsecte n'existe pas
        """
        self.unCatalogue.ajouterBoite(Boite("mangue douce", 0.10, 14, self.listeInsect["grillon"]))
        self.unCatalogue.ajouterBoite(Boite("oignon fumé", 0.10, 14, self.listeInsect["grillon"]))
        self.unCatalogue.ajouterBoite(Boite("paprika", 0.10, 10, self.listeInsect["criquet"]))
        self.unCatalogue.ajouterBoite(Boite("poivre & tomates séchées", 0.10, 10, self.listeInsect["criquet"]))
        self.unCatalogue.ajouterBoite(Boite("curry fruité", 0.01, 10, self.listeInsect["criquet"]))
        self.unCatalogue.ajouterBoite(Boite("à la grecque", 0.15, 10, self.listeInsect["criquet"]))
        self.unCatalogue.ajouterBoite(Boite("sésame & cumin", 0.10, 18, self.listeInsect["molitor"]))
        self.unCatalogue.ajouterBoite(Boite("ail & fines herbes", 0.10, 18, self.listeInsect["molitor"]))
        self.unCatalogue.ajouterBoite(Boite("soja impérial", 3, 10, self.listeInsect["molitor"]))

        nomInsecte="bernard"
        self.assertEqual(self.unCatalogue.lesBoitesDepuisNomInsecte(nomInsecte), [], "les boites ne sont pas celles souhaiter")